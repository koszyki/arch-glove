#!/bin/bash

docker run --rm -it \
	--name arch-glove \
	-v /certs:/certs \
	-v /etc/letsencrypt:/etc/letsencrypt \
	-p 80:80 \
	-p 443:443 \
	arch-glove

FROM archlinux/base

EXPOSE 80 443

RUN pacman -Sy && \
    yes | pacman -S certbot vim

VOLUME /certs

VOLUME /etc/letsencrypt

CMD bash

# Letsencrypt certs
So it's this time of the year again when you must get your
hands dirty on some ssl certificates and the good 'ol certbot.

If you're not adding any new domains, get into the arch-glove and run
```
certbot renew
```
otherwise, use
```
certbot certonly --standalone -d <domain-1> -d <domain-2> ...
```
